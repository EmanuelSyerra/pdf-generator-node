module.exports = {
    formate: 'Letter',
    orientation: 'portrait',
    border: '20mm',
    header: {
        height: '15mm',
        contents: '<h4 style="color:orange; font-size:20; font-weight:800; text-align:center;">REPORTE</h4>'
    },
    footer: {
        height: '15mm',
        contents: {
            first: 'Pág 1',
            2: 'Pág 2',
            default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', 
            last: 'Last Page'
        }
    }
}